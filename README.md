

Go语言轻量web开发框架，特点是一行代码搞定RESTFul，不依赖第三方ORM，也不需要生成一堆的controllers和models文件，快速使用。

## 背景
公司数据库有100多张表，用BeeGo等框架，需要生成controllers和models等文件共200多个，鼠标滑都滑不过来。每次表字段一有修改，代码就要统统跟着修改，想想都恐怖，于是就有了这个项目。支持增删改查的同时，针对増查支持批量操作，并集成了轻量的web开发框架。

## 快速使用

```go
package main

import (
	"net/http"
	"strconv"

	"gitee.com/hequan2020/go-restful-api"
)

func main() {

	// 初始化数据库连接
	if err := gogo.SQLInit("mysql", "数据库用户名:数据库密码@tcp(数据库地址:数据库端口)/表名?charset=utf8", 10, 1); err != nil {
	err != nil {
		gogo.Log().Error(err.Error())
		return
	}

	// 增
	gogo.POST("/restful/:tablename", func(ctx *gogo.HTTPContext) {
		affect, err := gogo.SQLInsert(
			ctx.GetPathParam(":tablename"),
			ctx.GetPostBody())

		if err != nil {
			gogo.Log().Error(err.Error())
			ctx.WriteString(err.Error())
		} else {
			ctx.WriteString(strconv.FormatInt(affect, 10))
		}
	})

	// 删
	gogo.DELETE("/restful/:tablename/:id", func(ctx *gogo.HTTPContext) {
		affect, err := gogo.SQLDelete(
			ctx.GetPathParam(":tablename"),
			"id="+ctx.GetPathParam(":id"))

		if err != nil {
			gogo.Log().Error(err.Error())
			ctx.WriteString(err.Error())
		} else {
			ctx.WriteString(strconv.FormatInt(affect, 10))
		}
	})

	// 改
	gogo.PUT("/restful/:tablename/:id", func(ctx *gogo.HTTPContext) {
		affect, err := gogo.SQLUpdate(
			ctx.GetPathParam(":tablename"),
			"id="+ctx.GetPathParam(":id"),
			ctx.GetPostBody())

		if err != nil {
			gogo.Log().Error(err.Error())
			ctx.WriteString(err.Error())
		} else {
			ctx.WriteString(strconv.FormatInt(affect, 10))
		}
	})

	// 查
	gogo.GET("/restful/:tablename/:id", func(ctx *gogo.HTTPContext) {
		queryData, err := gogo.SQLQueryByMap(
			"",
			ctx.GetString("feilds"),
			ctx.GetPathParam(":tablename"),
			"id="+ctx.GetPathParam(":id"),
			"", 0, 1)

		if err != nil {
			gogo.Log().Error(err.Error())
			ctx.WriteString(err.Error())
		} else {
			ctx.WriteJSON(queryData)
		}
	})

	// 查
	gogo.GET("/restful/:tablename", func(ctx *gogo.HTTPContext) {
		queryData, err := gogo.SQLQueryByMap(
			ctx.GetString("columnname"),
			ctx.GetString("feilds"),
			ctx.GetPathParam(":tablename"),
			ctx.GetString("where"),
			ctx.GetString("order"),
			ctx.GetInt("offset", 0),
			ctx.GetInt("count", 10))

		if err != nil {
			gogo.Log().Error(err.Error())
			ctx.WriteString(err.Error())
		} else {
			ctx.WriteJSON(queryData)
		}
	})

	// 404页面
	gogo.STATUS(http.StatusNotFound, func(ctx *gogo.HTTPContext) {
		ctx.WriteHeader(http.StatusNotFound)

		l, err := gogo.SQLQueryByMap("webname", "*", "dede_flink", "", "", 0, 100)
		if err == nil {
			ctx.WriteExecute(l,
				"views/notfound.tmpl")
			return
		}
	})

	// 启动HTTP服务
	gogo.Run(":3009")
}

```

## 数据库表结构
```mysql
mysql> desc dede_flink;
+----------+----------------------+------+-----+---------+----------------+
| Field    | Type                 | Null | Key | Default | Extra          |
+----------+----------------------+------+-----+---------+----------------+
| id       | smallint(5) unsigned | NO   | PRI | NULL    | auto_increment |
| sortrank | smallint(6)          | NO   |     | 0       |                |
| url      | char(60)             | NO   |     |         |                |
| webname  | char(30)             | NO   |     |         |                |
| msg      | char(200)            | NO   |     |         |                |
| email    | char(50)             | NO   |     |         |                |
| logo     | char(60)             | NO   |     |         |                |
| dtime    | int(10) unsigned     | NO   |     | 0       |                |
| typeid   | smallint(5) unsigned | NO   |     | 0       |                |
| ischeck  | smallint(6)          | NO   |     | 1       |                |
+----------+----------------------+------+-----+---------+----------------+
10 rows in set
```

## Windows
```
go build -o app.exe
app.exe
```

## Linux/MacOS
```
go build -o app
./app
```

## 浏览器访问测试

- 增
```
POST http://localhost:3009/restful/dede_flink
```
以下是application/json内容，可以批量添加多条数据
```
[
	{
    	"id": 20,
    	"ischeck": 1,
    	"webname": "hxyw",
    	"url":"http://www.hxyw.org"
	},
	{
    	"id": 21,
    	"ischeck": 1,
    	"webname": "bejson",
    	"url":"http://www.bejson.com"
	}
]
```

- 改
```
PUT http://localhost:3009/restful/dede_flink/21
```
以下是application/json内容，这里只将id=21的数据，webname字段修改为"hello"
```
{
    "webname": "hello"
}
```

- 查
```
GET http://localhost:3009/restful/dede_flink/21
GET http://localhost:3009/restful/dede_flink?order=-id
GET http://localhost:3009/restful/dede_flink?offset=10&&count=100&&columnname=webname
```

- 删
```
DELETE http://localhost:3009/restful/dede_flink/1
```

- 404
```
GET http://localhost:3009/weqwe
```

## Start using it

1. Download and install it:

```sh
$ go get github.com/go-sql-driver/mysql
$ go get github.com/phachon/go-logger
$ go get gitee.com/konyshe/gogo
```

2. Import it in your code:

```go
import "gitee.com/konyshe/gogo"
```

3. (Optional) Import `net/http`. This is required for example if using constants such as `http.StatusNotFound`.

```go
import "net/http"
```

## 示例中的方法介绍，更全的请看代码中的注释

```go
// 系统会优先判断是否命中固定URL，当固定URL无法命中的情况下，才会去判断是否命中正则URL(开发中部分支持)
gogo.GET("/restful/:tablename/:columnname", func(ctx *gogo.HTTPContext)

// 注册没有命中任何URL规则时进入的函数
gogo.STATUS(http.StatusNotFound, func(ctx *gogo.HTTPContext)

// SQLInit 初始化数据库操作句柄，这里要提供:
// driverName string: 数据库类型，例如mysql、sqlite等，参考github.com/go-sql-driver/mysql官方介绍
// dataSourceName string: 数据库地址，参考github.com/go-sql-driver/mysql官方介绍
// MaxOpenConns int: 最大缓存连接数，这个数值包含了MaxIdleConns
// MaxIdleConns int：预备的最大空闲连接数
gogo.SQLInit(driverName string, dataSourceName string, maxOpenConns int, maxIdleConns int) error

// SQLInsert 增加一条数据
// tableName string: 操作的表名
// data []byte: 需要更新的内容，用string转换后是json格式
gogo.SQLInsert(tableName string, data []byte) (int64, error)

// SQLDelete 根据where条件删除数据
// tableName string: 操作的表名
// where string: 过滤条件，就是where后面跟着的部分
gogo.SQLDelete(tableName, where string) (int64, error)

// SQLUpdate 更新一条数据
// tableName string: 操作的表名
// where string: 过滤条件，就是where后面跟着的部分
// data []byte: 需要更新的内容，用string转换后是json格式
gogo.SQLUpdate(tableName, where string, data []byte) (int64, error)

// SQLQueryByMap 将查询到的数据，按照指定字段的值做为索引构建map并返回
// columnName string: 作为索引的字段名称
// feilds string: 查询需要获取哪些字段的值，就是select后面跟着的部分，一般用"*"
// tableName string: 查询的表名
// where string: 过滤条件，就是where后面跟着的部分
// order string: 排序条件，就是order by后面跟着的部分。默认是ASC排序，除非"-"开头则DESC排序
// offset string: limit后面逗号相隔的两个数值，前者就是offset，后者就是count
// count string: limit后面逗号相隔的两个数值，前者就是offset，后者就是count
gogo.SQLQueryByMap(columname, feilds, tableName, where, order string, offset, count int) (interface{}, error)

// 输出日志到运行目录下的log目录下，同时控制台输出，还会根据日志级别显示不同颜色
// gogo.Log()返回的是go-logger的指针，后面的.Error()是go-logger的方法，具体使用参考github.com/phachon/go-logger
// content string: 需要输出的日志内容
// 如果你不喜欢，可以直接删掉LOGUtils.go文件
gogo.Log().Error(content string)

// 3009就是端口号了，这里和官方HTTP接口方法一致
gogo.Run(":3009")

// GetResponseWriter 获取原始的http.ResponseWriter指针
(ctx *HTTPContext) GetResponseWriter() http.ResponseWriter

// GetRequest 获取原始的http.Request指针
(ctx *HTTPContext) GetRequest() *http.Request

// GetPathParam 获取GET请求路径中的String格式参数值，例如/restful/:table_name/:id
// key string: 参数名称
(ctx *HTTPContext) GetPathParam(key string) string

// GetPostBody 获取POST请求的内容
(ctx *HTTPContext) GetPostBody() []byte

// WriteExecute 将官方原始的ParseFiles和Execute接口做了合并
// data interface{}: 渲染模板需要的数据
// filenames ...string: 模板文件路径
(ctx *HTTPContext) WriteExecute(data interface{}, filenames ...string) error

// WriteByte 将[]byte格式的数据输出给HTTP客户端
// content []byte: 需要输出HTTP客户端的[]byte格式数据
(ctx *HTTPContext) WriteByte(content []byte)

// WriteString 将String格式的数据输出给HTTP客户端
// content string: 需要输出HTTP客户端的String格式数据
(ctx *HTTPContext) WriteString(content string)

// WriteJSON 将Struct结构体的数据转换成Json输出给HTTP客户端
// v interface{}: 需要输出HTTP客户端的Struct结构体数据
(ctx *HTTPContext) WriteJSON(v interface{}) error
```

## 欢迎贡献者加入