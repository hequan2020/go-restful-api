package gogo

// The httpProcFunc type is an adapter to allow the use of
// ordinary functions as HTTP handlers. If f is a function
// with the appropriate signature, httpProcFunc(f) is a
// Handler that calls f.
type httpProcFunc func(*HTTPContext)

// ServeHTTP calls f(w, r).
func (f httpProcFunc) ServeHTTP(ctx *HTTPContext) {
	if ctx.r.Method == "GET" {
		ctx.r.ParseForm()
	}
	f(ctx)
}
