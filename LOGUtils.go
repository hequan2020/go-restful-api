package gogo

import (
	"os"

	"github.com/phachon/go-logger"
)

var (
	pLogger *go_logger.Logger
)

// init GoGo使用的第三方go_logger库，在此基础上固定了日志输出的路径为运行目录下log目录
func init() {
	if pLogger == nil {

		if _, err := os.Stat("log"); err != nil {
			os.Mkdir("log", 0777)
		}

		pLogger = go_logger.NewLogger()

		pLogger.Detach("console")

		// 命令行输出配置
		consoleConfig := &go_logger.ConsoleConfig{
			Color:      true,  // 命令行输出字符串是否显示颜色
			JsonFormat: false, // 命令行输出字符串是否格式化
			Format:     "",    // 如果输出的不是 json 字符串，JsonFormat: false, 自定义输出的格式
		}
		// 添加 console 为 logger 的一个输出
		pLogger.Attach("console", go_logger.LOGGER_LEVEL_DEBUG, consoleConfig)

		// 文件输出配置
		fileConfig := &go_logger.FileConfig{
			//Filename: "./log/test.log", // 日志输出文件名，不自动存在
			// 如果要将单独的日志分离为文件，请配置LealFrimeNem参数。
			LevelFileName: map[int]string{
				pLogger.LoggerLevel("error"): "./log/error.log", // Error 级别日志被写入 error .log 文件
				pLogger.LoggerLevel("info"):  "./log/info.log",  // Info 级别日志被写入到 info.log 文件中
				pLogger.LoggerLevel("debug"): "./log/debug.log", // Debug 级别日志被写入到 debug.log 文件中
			},
			MaxSize:    1024 * 1024, // 文件最大值（KB），默认值0不限
			MaxLine:    100000,      // 文件最大行数，默认 0 不限制
			DateSlice:  "d",         // 文件根据日期切分， 支持 "Y" (年), "m" (月), "d" (日), "H" (时), 默认 "no"， 不切分
			JsonFormat: false,       // 写入文件的数据是否 json 格式化
			Format:     "",          // 如果写入文件的数据不 json 格式化，自定义日志格式
		}
		// 添加 file 为 logger 的一个输出
		pLogger.Attach("file", go_logger.LOGGER_LEVEL_DEBUG, fileConfig)

		//pLogger.Info("this is a info log!")
		//pLogger.Errorf("this is a error %s log!", "format")
	}
}

// Log GoGo使用的第三方go_logger库，输出到日志文件的同时还会在控制台输出，并根据日志级别显示不同颜色
// 这里返回go_logger的指针，使用方法参考github.com/phachon/go-logger
func Log() *go_logger.Logger {
	return pLogger
}
